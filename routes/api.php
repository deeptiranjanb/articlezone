<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
* user router
*/

Route::name('me')->get('users/me', 'User\UserController@me');
Route::resource('users', 'User\UserController');
Route::name('verifyPhone')->post('users/verify-phone/', 'User\UserController@verifyPhone');
Route::name('resendOtp')->get('users/{user}/resend-otp', 'User\UserController@resendOtp');

Route::name('verify')->get('users/verify/{token}', 'User\UserController@verify');
Route::name('resend')->get('users/{user}/resend', 'User\UserController@resend');
Route::name('user.comments')->get('user/{user}/comments', 'Comment\UserCommentController');
Route::name('user.articles')->get('user/{user}/articles', 'Article\UserArticleController');

Route::name('googleuser')->post('googleuser', 'User\GoogleUserController@store');

/*
* articles router
*/

Route::resource('articles', 'Article\ArticleController');
Route::name('fullarticle')->get('fullarticle/{slug}', 'Article\FullArticleController');






/*
* comments router
*/

Route::resource('comments', 'Comment\CommentController');
Route::name('comment.article')->get('comment/article/{id}', 'Comment\ArticleCommentController');


/*
* catagory router
*/

Route::resource('categories', 'Category\CategoryController');
Route::name('category.articles')->get('category/articles/{slug}', 'Category\ArticleCategoryController');


/*
* auth router
*/
Route::post('oauth/token', '\Laravel\Passport\Http\Controllers\AccessTokenController@issueToken');

/*
* admin router
*/

Route::name('admin.details')->get('admin', 'Admin\AdminController');

/*
* contact us router
*/

Route::resource('contactus', 'Contactus\ContactusController');

/*
* mobile Number verification router
*/

Route::name('sendOtp')->post('/send-otp', 'MobileVerification\MobileVerificationController@sendOtp');
Route::name('verifyOtp')->post('/verify-otp', 'MobileVerification\MobileVerificationController@verifyOtp');

/**
 * payment controller
 */

Route::name('payment')->get('/stripe', 'Payment\PaymentController@index');