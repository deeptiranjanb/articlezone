<?php

namespace App\Traits;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\LengthAwarePaginator;

trait ApiResponser
{
    /**
     * Return Success message
     *
     * @param  array $data
     * @param  int $code
     * @return \Illuminate\Http\Response
     */

    private function successResponse($data, $code)
    {
        return response()->json($data, $code);
    }

    /**
     * Return error response
     *
     * @param  string $message
     * @param int $code
     * @return \Illuminate\Http\Response
     */

    protected function errorResponse($message, $code)
    {
        return response()->json(['error' => $message, 'code' => $code], $code);
    }

    /**
     * Return the collection as response
     *
     * @param  Collection $collection
     * @param int $code
     * @return \Illuminate\Http\Response
     */

    protected function showAll(Collection $collection, $code = 200)
    {
        if ($collection->isEmpty()) {
            return $this->successResponse(['data' => $collection], $code);
        }
        $transformer = $collection->first()->transformer;

        $collection = $this->filterData($collection, $transformer);
        $collection = $this->sortData($collection, $transformer);
        $collection = $this->paginate($collection);
        $collection = $this->transformData($collection, $transformer);
        $collection = $this->cacheResponse($collection);

        return $this->successResponse($collection, $code);
    }

    /**
     * Return the model instance as response
     *
     * @param  Model $model
     * @param int $code
     * @return \Illuminate\Http\Response
     */

    protected function showOne(Model $model, $code = 200)
    {
        $transformer = $model->transformer;

        $model = $this->transFormData($model, $transformer);

        return $this->successResponse($model, $code);
    }

    /**
     * Return a message as response
     *
     * @param  string $message
     * @param int $code
     * @return \Illuminate\Http\Response
     */

    protected function showMessage($message, $code = 200)
    {
        return $this->successResponse($message, $code);
    }

    /**
     * Filter the data based on query parameters
     *
     * @param  collection  $collection
     * @param Transformer $transformer
     * @return \Illuminate\Http\Response
     */

    protected function filterData(collection $collection, $transformer)
    {
        foreach (request()->query() as $query => $value) {

            $attribute = $transformer::originalAttributes($query);
            if (isset($attribute, $value)) {

                $collection = $collection->where($attribute, $value);
            }
        }
        return $collection;
    }

    /**
     * Sort the data based on query parameters
     *
     * @param  collection  $collection
     * @param Transformer $transformer
     * @return \Illuminate\Http\Response
     */

    protected function sortData(collection $collection, $transformer)
    {
        if (request()->has('sort_by')) {
            $attribute = $transformer::originalAttributes(request()->sort_by);
            $collection = $collection->sortBy->{$attribute};
        }
        return $collection;
    }

    /**
     * paginate the Collection
     *
     * @param  Collection $collection
     * @return \Illuminate\Http\Response
     */

    protected function paginate(Collection $collection)
    {
        $rules = [
            'per_page' => 'integer|min:2|max:21',
        ];

        Validator::validate(request()->all(), $rules);

        $page = LengthAwarePaginator::resolveCurrentPage();

        $perPage = 9;

        if (request()->has('per_page')) {
            $perPage = (int) request()->per_page;
        }
        if ($perPage == 21) {
            return $collection;
        }

        $results = $collection->slice(($page - 1) * $perPage, $perPage)->values();

        $paginated = new LengthAwarePaginator($results, $collection->count(), $perPage, $page, [
            'path' => LengthAwarePaginator::resolveCurrentPath(),
        ]);

        $paginated->appends(request()->all());

        return $paginated;
    }

    /**
     * Cache the response
     *
     * @param  Collection $data
     * @return \Illuminate\Http\Response
     */

    protected function cacheResponse($data)
    {

        $url = request()->url();
        $queryParams = request()->query();

        ksort($queryParams);

        $queryString = http_build_query($queryParams);

        $fullUrl = "{$url}?{$queryString}";

        return Cache::remember($fullUrl, 30 / 60, function () use ($data) {
            return $data;
        });
    }

    /**
     * transorm Collection keys
     *
     * @param  Collection $data
     * @param Transformer $transformer
     * @return \Illuminate\Http\Response
     */

    protected function transformData($data, $transformer)
    {
        $transformation = fractal($data, new $transformer);

        return $transformation->toArray();
    }
}