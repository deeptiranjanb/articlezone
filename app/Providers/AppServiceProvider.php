<?php

namespace App\Providers;


use App\Model\User;
use App\Model\Article;
use App\Model\Comment;
use App\Mail\UserCreated;
use App\Mail\CommentAdded;
use App\Mail\UserMailChanged;
use Illuminate\Mail\Mailable;
use App\Mail\ArticleSubmitted;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Schema::defaultStringLength(191);

        //Send an email to admin on article submission

        Article::created(function ($article) {
            retry(5, function () use ($article) {
                $admin = User::get()->where('is_admin', 'true')->first();
                Mail::to($admin)->send(new ArticleSubmitted($admin));
            }, 100);
        });

        //Send an email to author if a new comment is submitted on the article

        Comment::created(function ($comment) {
            retry(5, function () use ($comment) {
                if ($comment->user_id) {
                    $article = Article::findOrFail($comment->article_id);
                    $user = User::findOrFail($article->user_id);
                    Mail::to($user)->send(new CommentAdded($user));
                }
            }, 100);
        });

        //Send an verification email to user on creation

        User::created(function ($user) {
            retry(5, function () use ($user) {
                Mail::to($user)->send(new UserCreated($user));
            }, 100);
        });

        //Send an verification email to user if email is updated

        User::updated(function ($user) {
            if ($user->isDirty('email')) {
                retry(5, function () use ($user) {
                    Mail::to($user)->send(new UserMailChanged($user));
                }, 100);
            }
        });
    }
}