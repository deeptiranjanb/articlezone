<?php

namespace App\Model;

use App\Model\Article;
use App\Model\Comment;

use Laravel\Passport\HasApiTokens;
use App\Transformers\UserTransformer;

use Illuminate\Database\Eloquent\Model;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable

{
    use Notifiable, HasApiTokens, SoftDeletes;
    protected $dataes = ['deleted_at'];
    public $transformer = UserTransformer::class;


    const VERIFIED_USER = '1';
    const UNVERIFIED_USER = '0';

    const ADMIN_USER = 'true';
    const REGULAR_USER = 'false';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'mobile_no', 'password', 'is_admin', 'verified', 'verification_token', 'auth_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', //'verification_token'
    ];


    public function isVerified()
    {
        return $this->verified == User::VERIFIED_USER;
    }

    public function isAdmin()
    {
        return $this->is_admin == User::ADMIN_USER;
    }

    public static function generateVerificationCode()
    {
        return  rand(100000, 999999);
    }
    public function articles()
    {
        return $this->hasMany(Article::class);
    }
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
}