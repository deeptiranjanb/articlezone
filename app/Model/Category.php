<?php

namespace App\Model;


use Illuminate\Database\Eloquent\Model;
use App\Transformers\CategoryTransformer;
use App\Model\Article;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    //
    use SoftDeletes;
    protected $dataes = ['deleted_at'];

    protected $fillable = [
        'name',
        'slug'
    ];
    public $transformer = CategoryTransformer::class;

    public function articles()
    {
        return $this->belongsToMany(Article::class)->withTimestamps();
    }
}