<?php

namespace App\Model;


use App\Model\User;
use App\Model\Category;
use Illuminate\Database\Eloquent\Model;
use App\Transformers\ArticleTransformer;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article extends Model
{
    //
    use SoftDeletes;
    protected $dataes = ['deleted_at'];
    public $transformer = ArticleTransformer::class;


    const APPROVED_ARTICLE = '1';
    const UNAPPROVED_ARTICLE = '0';
    protected $fillable = [
        'slug',
        'title',
        'excerpt',
        'content',
        'image',
        'user_id',
        'comment_count',
        'status'
    ];

    /**
     * Check wheather the user is verified or not
     *
     * @return $this
     */

    public function isVerified()
    {
        return $this->status == Article::APPROVED_ARTICLE;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function categories()
    {
        return $this->belongsToMany(Category::class)->withTimestamps();
    }
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
}