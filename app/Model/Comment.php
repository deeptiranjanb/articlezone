<?php

namespace App\Model;


use App\Model\User;
use App\Model\Article;
use Illuminate\Database\Eloquent\Model;
use App\Transformers\CommentTransformer;
use Illuminate\Database\Eloquent\SoftDeletes;


class Comment extends Model
{
    //
    use SoftDeletes;
    protected $dataes = ['deleted_at'];

    public $transformer = CommentTransformer::class;

    const APPROVED_COMMENT = '1';
    const UNAPPROVED_COMMENT = '0';
    protected $fillable = [
        'article_id',
        'user_id',
        'comment_body',
        'status'
    ];
    public function isApproved()
    {
        return $this->status == Comment::APPROVED_COMMENT;
    }
    public function article()
    {
        return $this->belongsTo(Article::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}