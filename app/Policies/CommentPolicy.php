<?php

namespace App\Policies;

use App\Model\User;
use App\Model\Comment;
use App\Traits\AdminActions;
use Illuminate\Auth\Access\HandlesAuthorization;

class CommentPolicy
{
    use HandlesAuthorization, AdminActions;



    /**
     * Determine whether the user can view the comment.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Model\Comment  $comment
     * @return mixed
     */
    public function view(User $user, Comment $comment)
    {
        //
        return $user->id === $comment->user_id;
    }

    /**
     * Determine whether the user can update the comment.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Model\Comment  $comment
     * @return mixed
     */
    public function update(User $user, Comment $comment)
    {
        //
        return $user->id === $comment->user_id;
    }

    /**
     * Determine whether the user can delete the comment.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Comment  $comment
     * @return mixed
     */
    public function delete(User $user, Comment $comment)
    {
        //
        return $user->id === $comment->$user->id;
    }
}