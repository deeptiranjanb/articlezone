<?php

namespace App\Policies;

use App\Model\User;
use App\Model\Article;
use App\Traits\AdminActions;
use Illuminate\Auth\Access\HandlesAuthorization;

class ArticlePolicy
{
    use HandlesAuthorization, AdminActions;

    /**
     * Determine whether the user can view any articles.
     *
     * @param  \App\Model\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the article.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Article  $article
     * @return mixed
     */
    public function view(User $user, Article $article)
    {
        //
        return $user->id === $article->user_id;
    }



    /**
     * Determine whether the user can update the article.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Article  $article
     * @return mixed
     */
    public function update(User $user, Article $article)
    {
        //
        return $user->id === $article->user_id;
    }

    /**
     * Determine whether the user can delete the article.
     *
     * @param  \App\Model\User  $user
     * @param  \App\Article  $article
     * @return mixed
     */
    public function delete(User $user, Article $article)
    {
        //
        return $user->id === $article->user_id;
    }
}