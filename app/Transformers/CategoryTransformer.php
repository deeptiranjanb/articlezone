<?php

namespace App\Transformers;


use App\Model\Category;
use League\Fractal\TransformerAbstract;

class CategoryTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Category $category)
    {
        return [
            //
            'identifier' => $category->id,
            'title' => $category->name,
            'slug' => $category->slug,
            'creationDate' => $category->created_at,
            'lastChange' => $category->updated_at,
            'articleCount' => $category->articles->count(),
            'deleteChange' => isset($category->deleted_at) ? (string) $category->deleted_at : null,
        ];
    }

    /**
     * Return original attributes
     *
     * @return array
     */

    public static function originalAttributes($index)
    {
        $attributes = [
            'identifier' => 'id',
            'title' => 'name',
            'slug' => 'slug',
            'creationDate' => 'created_at',
            'lastChange' => 'updated_at',
            'deleteChange' => 'deleted_at',
            'articleCount' => 'article_count'
        ];
        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    /**
     * Return transformed attributes
     *
     * @return array
     */

    public static function transformedAttributes($index)
    {
        $attributes = [
            'id' => 'identifier',
            'title' => 'name',
            'slug' => 'slug',
            'article_count' => 'articleCount',
            'created_at' => 'creationDate',
            'updated_at' => 'lastChange',
            'deleted_at' => 'deleteChange'
        ];
        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}