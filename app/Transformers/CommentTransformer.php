<?php

namespace App\Transformers;


use App\Model\Comment;
use League\Fractal\TransformerAbstract;

class CommentTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Comment $comment)
    {
        return [
            //
            'identifier' => $comment->id,
            'article' => $comment->article_id,
            'user' => $comment->user_id,
            'commentContent' => $comment->comment_body,
            'isApproved' => ($comment->status    === '1'),
            'creationDate' => $comment->created_at,
            'lastChange' => $comment->updated_at,
            'deleteChange' => isset($comment->deleted_at) ? (string) $comment->deleted_at : null,
        ];
    }

    /**
     * Return original atrributes
     *
     * @return array
     */

    public static function originalAttributes($index)
    {
        $attributes = [
            'identifier' => 'id',
            'article' => 'article_id',
            'user' => 'user_id',
            'commentContent' => 'comment_body',
            'isApproved' => 'status',
            'creationDate' => 'created_at',
            'lastChange' => 'updated_at',
            'deleteChange' => 'deleted_at'
        ];
        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    /**
     * Return transformed attribute
     *
     * @return array
     */


    public static function transformedAttributes($index)
    {
        $attributes = [
            'id' => 'identification',
            'article_id' => 'article',
            'user_id' => 'user',
            'comment_body' => 'commentContent',
            'status' => 'isApproved',
            'created_at' => 'creationDate',
            'updated_at' => 'lastChange',
            'deleted_at' => 'deleteChange'
        ];
        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}