<?php

namespace App\Transformers;


use App\Model\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(User $user)
    {
        return [
            //
            'identifier' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'mobileNo' => $user->mobile_no,
            'isVerified' => (int) $user->verified,
            'isAdmin' => ($user->is_admin === 'true'),
            'creationDate' => $user->created_at,
            'lastChange' => $user->updated_at,
            'deleteChange' => isset($user->deleted_at) ? (string) $user->deleted_at : null,
        ];
    }

    /**
     * Return original atrributes
     *
     * @return array
     */

    public static function originalAttributes($index)
    {
        $attributes = [
            'identifier' => 'id',
            'name' => 'name',
            'email' => 'email',
            'password' => 'password',
            'password_confirmation' => 'password_confirmation',
            'mobileNo' => 'mobile_no',
            'isVerified' => 'verified',
            'isAdmin' => 'is_admin',
            'creationDate' => 'created_at',
            'lastChange' => 'updated_at',
            'deleteChange' => 'deleted_at'
        ];
        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    /**
     * Return transformed atrributes
     *
     * @return array
     */

    public static function transformedAttributes($index)
    {
        $attributes = [
            'id' => 'identifier',
            'name' => 'name',
            'email' => 'email',
            'password' => 'password',
            'mobile_no' => 'mobileNo',
            'verified' => 'isVerified',
            'is_admin' => 'isAdmin',
            'creationDate' => 'created_at',
            'lastChange' => 'updated_at',
            'deleteChange' => 'deleted_at'
        ];
        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}