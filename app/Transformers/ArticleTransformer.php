<?php

namespace App\Transformers;


use App\Model\Article;
use League\Fractal\TransformerAbstract;

class ArticleTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];


    public function transform(Article $article)
    {
        return [
            //
            'identifier' => $article->id,
            'title' => $article->title,
            'excerpt' => $article->excerpt,
            'content' => $article->content,
            'slug' => $article->slug,
            'author' => $article->user_id,
            'image' => $article->image,
            'commentCount' => $article->comment_count,
            'approvalStatus' => ($article->status    === '1'),
            'creationDate' => (string) $article->created_at,
            'lastChange' => (string) $article->updated_at,
            'deleteChange' => isset($article->deleted_at) ? (string) $article->deleted_at : null,
        ];
    }

    /**
     * Return original attributes
     *
     * @return array
     */

    public static function originalAttributes($index)
    {
        $attributes = [
            'identifier' => 'id',
            'title' => 'title',
            'excerpt' => 'excerpt',
            'content' => 'content',
            'slug' => 'slug',
            'author' => 'user_id',
            'image' => 'image',
            'categories' => 'categories',
            'commentCount' => 'comment_count',
            'approvalStatus' => 'status',
            'creationDate' => 'created_at',
            'lastChange' => 'updated_at',
            'deleteChange' => 'deleted_at'
        ];
        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    /**
     * Return transformed attributes
     *
     * @return array
     */

    public static function transformedAttributes($index)
    {
        $attributes = [
            'id' => 'identifier',
            'title' => 'title',
            'excerpt' => 'excerpt',
            'content' => 'content',
            'categories' => 'categories',
            'slug' => 'slug',
            'user_id' => 'author',
            'image' => 'image',
            'comment_count' => 'commentCount',
            'status' => 'approvalStatus',
            'created_at' => 'creationDate',
            'updated_at' => 'lastChange',
            'deleted_at' => 'deleteChange'
        ];
        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}