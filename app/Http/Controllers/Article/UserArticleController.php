<?php

namespace App\Http\Controllers\Article;


use App\Model\User;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Transformers\ArticleTransformer;
use ParentIterator;

class UserArticleController extends ApiController
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware('client.credentials');
        $this->middleware('can:view,user')->only('__invoke');
    }

    /**
     * Display all the articles by an user
     *
     * @param  User $user
     * @return \Illuminate\Http\Response
     */

    public function __invoke(User $user)
    {
        //
        $articles = $user->articles;
        return $this->showAll($articles);
    }
}