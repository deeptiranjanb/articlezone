<?php

namespace App\Http\Controllers\Article;


use App\Model\User;
use App\Model\Article;
use Illuminate\Http\Request;
use App\Http\Requests\StoreArticle;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateArticle;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Storage;
use App\Transformers\ArticleTransformer;



class ArticleController extends ApiController
{
    public function __construct()
    {
        // parent::__construct();
        $this->middleware('auth:api')->except(['index', 'store']);
        $this->middleware('transform.input:' . ArticleTransformer::class)->only(['store', 'update']);
        $this->middleware('client.credentials')->only(['show']);
        $this->middleware('can:update,article')->only('update');
        $this->middleware('can:delete,article')->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $articles = Article::all()->sortByDesc('comment_count');
        return $this->showAll($articles);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreArticle $request)
    {
        //

        $data = $request->validated();
        $slug = str_slug($data['title']);

        if (isset($data['user_id'])) {
            $user = User::findOrFail($data['user_id']);
            if ($user->isVerified()) {
                $data['status'] = Article::APPROVED_ARTICLE;
                $data['published_at'] = now();
            }
        } else {
            $data['status'] = Article::UNAPPROVED_ARTICLE;
        }

        $data['image'] = $request->image->store('');


        $data['comment_count'] = 0;
        $slug = str_slug($data['title']);
        $data['slug'] = $slug;

        $article = Article::create($data);
        if (isset($data['categories'])) {
            $categories = array_map('intval', explode(',', $data['categories']));
            unset($data['categories']);
            $article->categories()->sync($categories);
        }



        return $this->showOne($article);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        //
        $response['article'] = $this->showOne($article)->getData()->data;
        $response['categories'] = $this->showAll($article->categories)->getData()->data;

        return response()->json($response);
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Article $article
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateArticle $request, Article $article)
    {
        //
        // $this->validate($request, $rules);
        $data = $request->validated();


        if (isset($data['title'])) {
            $slug = str_slug($request->title);
            $article->title = $request->title;
            $article->slug = $slug;
        }

        if (isset($data['excerpt'])) {
            $article->excerpt = $request->excerpt;
        }

        if (isset($data['content'])) {
            $article->content = $request->content;
        }

        if (isset($data['categories'])) {
            $categories = array_map('intval', explode(',', $data['categories']));
            $article->categories()->sync($categories);
        }
        if (isset($data['status'])) {
            if ($data['status'] == 'true') {
                $article->status = '1';
            } else if ($data['status'] == 'false') {
                $article->status = '0';
            }
        }

        if ($request->hasFile('image')) {
            Storage::delete($article->image);

            $article->image = $request->image->store('');
        }

        if (!$article->isDirty()) {
            return $this->errorResponse('You need to at least update one field', 422);
        }
        $article->save();
        return $this->showOne($article);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  object App\Article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        //
        Storage::delete($article->image);
        $article->delete();
        return $this->showOne($article);
    }

    /**
     * Approve an article
     *
     * @param  \Illuminate\Http\Request  $request
     * @param User $user
     * @return \Illuminate\Http\Response
     */

    public function aprroveArticle(Article $article, User $user)
    {

        if ($article->isApproved() && $user->isAdmin()) {
            $article->status = Article::APPROVED_ARTICLE;
        }
    }
}