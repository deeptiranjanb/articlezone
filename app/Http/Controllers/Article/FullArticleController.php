<?php

namespace App\Http\Controllers\Article;


use App\Model\User;
use App\Model\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class FullArticleController extends ApiController
{
    public function __construct()
    {
        // parent::__construct();
    }
    /**
     * show full article with comment and catagories
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function __invoke($slug)
    {
        //
        $article = Article::get()->where('slug', $slug)->first();

        $comments = $article->comments->where('status', '1');
        $users = [];
        if ($article->user_id) {
            $users['article'] = User::where('id', $article->user_id)->first()->name;
        }
        foreach ($comments as $comment) {
            if ($comment->user_id) {
                $users['comment'][User::where('id', $comment->user_id)->first()->id] = User::where('id', $comment->user_id)->first()->name;
            }
        }
        $categories = $article->categories;

        $response = [
            'article' => $this->showOne($article)->getData()->data,
            'comments' => $this->showAll($comments)->getData()->data,
            'categories' => $this->showAll($categories)->getData()->data,
            'userNames' => $users
        ];

        return response()->json($response, 200);
    }
}