<?php

namespace App\Http\Controllers\Contactus;

use App\Model\Contactus;
use Illuminate\Http\Request;
use App\Http\Requests\StoreUser;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreContactus;

class ContactusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $contactData = Contactus::all();
        return response()->json($contactData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreContactus $request)
    {
        //
        $data = $request->validated();

        $contactData = Contactus::create($data);
        return response()->json($contactData);
    }
}