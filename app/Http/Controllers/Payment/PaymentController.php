<?php

namespace App\Http\Controllers\Payment;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Stripe;

class PaymentController extends Controller
{
    //
    /**
     * Return the client secret for payment
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        \Stripe\Stripe::setApiKey(getenv("STRIPE_SECRET"));

        $intent = \Stripe\PaymentIntent::create([
            'amount' => 10000,
            'currency' => 'inr',
            // Verify your integration in this guide by including this parameter
            'metadata' => ['integration_check' => 'accept_a_payment'],
        ]);
        return response()->json(['client_secret' => $intent->client_secret]);
    }
}