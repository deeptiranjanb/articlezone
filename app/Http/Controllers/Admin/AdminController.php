<?php

namespace App\Http\Controllers\Admin;

use App\Model\User;
use App\Model\Article;
use App\Model\Comment;
use App\Model\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class AdminController extends ApiController
{
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function __invoke(Request $request)
    {
        $this->allowedAdminAction();
        $verifiedUsers = User::get()->where('verified', '1')->count();
        $adminUsers = User::get()->where('is_admin', 'true')->count();
        $unverifiedUsers = User::get()->where('verified', '0')->count();
        $approvedArticles = Article::get()->where('status', '1')->count();
        $unapprovedArticles = Article::get()->where('status', '0')->count();
        $approvedComments = Comment::get()->where('status', '1')->count();
        $unapprovedComments = Comment::get()->where('status', '0')->count();
        $categories = Category::all()->count();
        $response = compact(
            'verifiedUsers',
            'unverifiedUsers',
            'adminUsers',
            'approvedArticles',
            'unapprovedArticles',
            'approvedComments',
            'unapprovedComments',
            'categories'
        );
        return response()->json($response);
    }
}