<?php

namespace App\Http\Controllers\Comment;

use App\Model\User;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class UserCommentController extends ApiController
{

    public function __construct()
    {

        parent::__construct();
        $this->middleware('client.credentials');
        $this->middleware('can:view,user')->only('__invoke');
    }
    /**
     * Return all the comments by an user
     *
     * @param  User $user
     * @return \Illuminate\Http\Response
     */
    public function __invoke(User $user)
    {
        $comments = $user->comments;
        return $this->showAll($comments);
    }
}