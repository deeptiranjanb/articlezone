<?php

namespace App\Http\Controllers\Comment;


use App\Model\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class ArticleCommentController extends ApiController
{
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Show the article for a particular comment
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function __invoke($id)
    {
        //
        $comment = Comment::findOrFail($id);
        $article = $comment->article;
        return $this->showOne($article);
    }
}