<?php

namespace App\Http\Controllers\Comment;



use App\Model\User;
use App\Model\Article;
use App\Model\Comment;
use Illuminate\Http\Request;
use App\Http\Requests\StoreComment;
use App\Http\Controllers\ApiController;
use App\Http\Requests\UpdateComment;
use App\Transformers\CommentTransformer;

class CommentController extends ApiController
{
    public function __construct()
    {
        // parent::__construct();
        $this->middleware('auth:api')->except(['store']);
        $this->middleware('transform.input:' . CommentTransformer::class)->only(['store', 'update']);
        $this->middleware('client.credentials')->only(['show']);
        //$this->middleware('can:update,comment')->only(['update']);
        $this->middleware('can:delete,comment')->only(['delete']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $this->allowedAdminAction();
        $comments = Comment::all();
        return $this->showAll($comments);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreComment $request)
    {
        //

        $data = $request->validated();

        $article = Article::findOrFail($request->article_id);
        $article->comment_count += 1;
        $article->save();

        if (isset($data['user_id'])) {
            $user = User::findOrFail($data['user_id']);
            if ($user->isVerified()) {
                $data['status'] = Comment::APPROVED_COMMENT;
                $data['approved_at'] = now();
            }
        } else {

            $data['status'] = Comment::UNAPPROVED_COMMENT;
        }

        $comment = Comment::create($data);

        return $this->showOne($comment);
    }

    /**
     * Display the specified resource.
     *
     * @param  object Comment class
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        //
        return $this->showOne($comment);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateComment $request, Comment $comment)
    {

        $data = $request->validated();

        $comment->comment_body = $data['comment_body'];

        if (isset($data['status'])) {
            if ($data['status'] == 'true') {
                $comment->status = '1';
            } else if ($data['status'] == 'true') {
                $comment->status = '0';
            }
        }

        $comment->save();

        return  $this->showOne($comment);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  object $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        //
        $article = Article::findOrFail($comment->article_id);
        $article->comment_count -= 1;
        $article->save();
        $comment->delete();

        return $this->showOne($comment);
    }
}