<?php

namespace App\Http\Controllers\Category;


use App\Model\Article;
use App\Model\Category;
use Illuminate\Http\Request;
use App\Http\Requests\StoreCategory;
use App\Http\Controllers\ApiController;
use App\Http\Requests\UpdateCategory;
use App\Transformers\CategoryTransformer;

class CategoryController extends ApiController
{
    public function __construct()
    {
        //parent::__construct();
        $this->middleware('auth:api')->except(['index']);

        $this->middleware('transform.input:' . CategoryTransformer::class)->only(['store', 'update', 'delete']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Show  all categories
        $categories = Category::all();
        return $this->showAll($categories);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCategory $request)
    {

        $this->allowedAdminAction();
        $data = $request->validated();

        $data['slug'] = str_slug($data['name']);

        $category = Category::create($data);

        return $this->showOne($category);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {

        $categories = category::get()->where('slug', $slug);


        return $this->showAll($categories);
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCategory $request, Category $category)
    {
        //

        $this->allowedAdminAction();
        $data = $request->validated();

        $category->name = $data['name'];
        $category->slug = str_slug($data['name']);

        $category->save();

        return $this->showOne($category);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  object App\category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
        $this->allowedAdminAction();
        $articlesCount = $category->articles->count();

        if (!$articlesCount) {
            $category->delete();
            return $this->showOne($category);
        } else {
            return $this->errorResponse('This category is linked to an article. So it can\'t be deleted.', 422);
        }
    }
}