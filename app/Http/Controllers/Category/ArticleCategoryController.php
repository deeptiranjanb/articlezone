<?php

namespace App\Http\Controllers\Category;


use App\Model\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class ArticleCategoryController extends ApiController
{
    public function __construct()
    {
        // parent::__construct();
    }
    /**
     * Handle the incoming request.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */

    public function __invoke($slug)
    {

        $category = Category::get()->where('slug', $slug)->first();
        $articles = $category->articles;

        return $this->showAll($articles);
    }
}