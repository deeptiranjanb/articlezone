<?php


namespace App\Http\Controllers\User;

use Google_Client;
use App\Model\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GoogleUserController extends Controller
{

    //
    /**
     * Verify the google user with id token
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $CLIENT_ID = getenv('GOOGLE_CLIENT_ID');
        $client = new Google_Client(['client_id' => $CLIENT_ID]);  // Specify the CLIENT_ID of the app that accesses the backend
        $payload = $client->verifyIdToken($request->id_token);
        if ($payload) {
            $userid = $payload['sub'];
            $data['name'] = $payload['name'];
            $data['email'] = $payload['email'];
            $data['password'] = bcrypt($payload['sub']);
            $data['verified'] = User::VERIFIED_USER;
            $data['is_admin'] = User::REGULAR_USER;

            $user = User::create($data);
            $user->user_verified_at = now();
            $user->save();
            return response()->json($user, 201);
        } else {
            // Invalid ID token
            return response()->json('no user found', 400);
        }
    }
}