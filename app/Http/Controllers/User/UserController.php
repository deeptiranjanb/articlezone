<?php

namespace App\Http\Controllers\User;


use App\Model\User;
use Twilio\Rest\Client;
use App\Mail\UserCreated;
use Illuminate\Http\Request;
use App\Http\Requests\StoreUser;
use App\Http\Requests\UpdateUser;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Mail;
use App\Transformers\UserTransformer;
use App\Http\Controllers\ApiController;
use Illuminate\Auth\Access\AuthorizationException;

class UserController extends ApiController
{
    public function __construct()
    {

        $this->middleware('auth:api')->except(['store', 'resend', 'verify', 'verifyPhone', 'resendOtp']);
        $this->middleware('client.credentials')->only(['resend']);
        $this->middleware('transform.input:' . UserTransformer::class)->only(['store', 'update']);


        $this->middleware('can:view,user')->only('show');
        $this->middleware('can:update,user')->only('update');
        $this->middleware('can:delete,user')->only('delete');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->allowedAdminAction();
        $users = User::all();
        return $this->showAll($users);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUser $request)
    {
        //

        $data = $request->validated();

        $token = getenv("TWILIO_AUTH_TOKEN");
        $twilio_sid = getenv("TWILIO_SID");
        $twilio_verify_sid = getenv("TWILIO_VERIFY_SID");
        $twilio = new Client($twilio_sid, $token);
        $twilio->verify->v2->services($twilio_verify_sid)
            ->verifications
            ->create($data['mobile_no'], "sms");

        $data['password'] = bcrypt($request->password);
        $data['verified'] = User::UNVERIFIED_USER;
        $data['verification_token'] = User::generateVerificationCode();
        $data['is_admin'] = User::REGULAR_USER;

        $user = User::create($data);

        return $this->showOne($user, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
        //$user = User::findOrFail($id);
        return $this->showOne($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  object App/User
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUser $request, User $user)
    {

        $data = $request->validated();

        if (isset($data['name'])) {
            $user->name = $data['name'];
        }

        if (isset($data['email'])) {
            $user->verified = User::UNVERIFIED_USER;
            $user->verification_token = User::generateVerificationCode();
            $user->email = $data['email'];
        }
        if (isset($data['mobile_no'])) {
            $user->verified = User::UNVERIFIED_USER;
            $token = getenv("TWILIO_AUTH_TOKEN");
            $twilio_sid = getenv("TWILIO_SID");
            $twilio_verify_sid = getenv("TWILIO_VERIFY_SID");
            $twilio = new Client($twilio_sid, $token);
            $twilio->verify->v2->services($twilio_verify_sid)
                ->verifications
                ->create($data['mobile_no'], "sms");
        }
        if (isset($data['password'])) {
            $user->password = bcrypt($data['password']);
        }
        if (isset($data['is_admin'])) {
            $this->allowedAdminAction();
            $user->is_admin = $data['is_admin'];
        }
        if (!$user->isDirty()) {
            return $this->errorResponse('You need to update at least one field', 422);
        }
        $user->save();

        return $this->showOne($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
        //   $user = User::findOrFail($id);
        $user->delete();

        return $this->showOne($user);
    }

    /**
     * Return the authenticated user
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function me(Request $request)
    {
        $user = $request->user();

        return $this->showOne($user);
    }

    /**
     * Verify the user
     *
     * @param  string $token
     * @return \Illuminate\Http\Response
     */

    public function verify($token)
    {

        $user = User::where('verification_token', $token)->firstOrFail();

        $user->verified = User::VERIFIED_USER;
        $user->verification_token = null;
        $user->user_verified_at = now();

        $user->save();

        return $this->showMessage('the account has been verified successfully');
    }

    /**
     * Verify the user mobile number
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    protected function verifyPhone(Request $request)
    {
        $data = $request->validate([
            'verification_code' => ['required', 'numeric'],
            'mobileNo' => ['required', 'string'],
        ]);
        /* Get credentials from .env */
        $token = getenv("TWILIO_AUTH_TOKEN");
        $twilio_sid = getenv("TWILIO_SID");
        $twilio_verify_sid = getenv("TWILIO_VERIFY_SID");
        $twilio = new Client($twilio_sid, $token);
        $verification = $twilio->verify->v2->services($twilio_verify_sid)
            ->verificationChecks
            ->create($data['verification_code'], array('to' => $data['mobileNo']));

        if ($verification->valid) {
            $user = User::where('mobile_no', $data['mobileNo'])->first();
            $user->verified = User::VERIFIED_USER;
            $user->user_verified_at = now();
            $user->save();
            return $this->showMessage('the account has been verified successfully');
        }
    }

    /**
     * Resend the verification Email
     *
     * @param  User $user
     * @return \Illuminate\Http\Response
     */

    public function resend(User $user)
    {
        if ($user->isVerified()) {
            return $this->errorResponse('This user is already verified', 409);
        }

        retry(5, function () use ($user) {
            Mail::to($user)->send(new UserCreated($user));
        }, 100);
        return $this->showMessage('The verification email has been resend');
    }

    /**
     * Resend the verification otp
     *
     * @param  User $user
     * @return \Illuminate\Http\Response
     */

    public function resendOtp(User $user)
    {
        if ($user->isVerified()) {
            return $this->errorResponse('This user is already verified', 409);
        }

        $token = getenv("TWILIO_AUTH_TOKEN");
        $twilio_sid = getenv("TWILIO_SID");
        $twilio_verify_sid = getenv("TWILIO_VERIFY_SID");
        $twilio = new Client($twilio_sid, $token);
        $twilio->verify->v2->services($twilio_verify_sid)
            ->verifications
            ->create($user->mobile_no, "sms");

        return $this->showMessage('The otp has been resend');
    }
}