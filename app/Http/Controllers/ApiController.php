<?php

namespace App\Http\Controllers;

use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Auth\Access\AuthorizationException;

class ApiController extends Controller
{

    use ApiResponser;
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Allow only admin
     */

    protected function allowedAdminAction()
    {
        if (Gate::denies('admin-action')) {
            throw new AuthorizationException('Only admin has this level of access');
        }
    }
}