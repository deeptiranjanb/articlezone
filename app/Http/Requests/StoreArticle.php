<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreArticle extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'title' => 'bail|required|unique:articles|max:250',
            'excerpt' => 'bail|required',
            'content' => 'bail|required',
            'categories' => 'bail|required',
            'user_id' => 'nullable|Numeric',
            'image' => 'bail|image|max:4096'
        ];
    }
}
