<?php

namespace App\Http\Requests;

use App\Model\User;
use Illuminate\Foundation\Http\FormRequest;

class UpdateUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => "email|unique:users,email,$this->id,id",
            'mobile_no' => "numeric|unique:users,mobile_no,$this->id,id",
            'password' => 'min:6|confirmed',
            'is_admin' => 'in:' . User::ADMIN_USER . ',' . User::REGULAR_USER,
        ];
    }
}