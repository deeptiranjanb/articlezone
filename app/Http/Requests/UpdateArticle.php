<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateArticle extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'title' => "unique:articles,title,$this->id,id",
            'excerpt' => '',
            'content' => '',
            'categories' => '',
            'status' => 'nullable',
            'user_id' => 'nullable',
            'image' => 'image|max:4096'

        ];
    }
}