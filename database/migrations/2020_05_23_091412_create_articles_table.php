<?php

use App\Model\Article;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->unique();
            $table->text('excerpt');
            $table->longText('content');
            $table->string('slug')->unique();
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->string('status')->default(Article::APPROVED_ARTICLE);
            $table->string('image')->nullable();
            $table->dateTime('published_at')->nullable();
            $table->integer('comment_count')->unsigned()->defalut(0);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}