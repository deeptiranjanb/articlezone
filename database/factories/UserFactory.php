<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */



use App\Model\User;
use App\Model\Article;
use App\Model\Comment;
use App\Model\Category;
use Illuminate\Support\Str;

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/


/**
 * Return User Factory
 *
 * @return array
 */

$factory->define(User::class, function (Faker $faker) {
    static $password;
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'user_verified_at' => now(),
        'mobile_no' => $faker->unique()->phoneNumber,
        'password' => $password ?: $password = bcrypt('secret'), // password
        'remember_token' => Str::random(10),
        'verified' => $verified = $faker->randomElement([User::VERIFIED_USER, User::UNVERIFIED_USER]),
        'verification_token' => $verified == User::VERIFIED_USER ? null : User::generateVerificationCode(),
        'is_admin' => $isAdmin = $faker->randomElement([User::REGULAR_USER, User::ADMIN_USER]),

    ];
});

/**
 * Return Category factory
 *
 * @return array
 */

$factory->define(Category::class, function (Faker $faker) {
    $name = $faker->unique()->word;
    $slug = str_slug($name);

    return [
        'name' => $name,
        'slug' => $slug,

    ];
});

/**
 * Return Article factory
 *
 * @return array
 */

$factory->define(Article::class, function (Faker $faker) {
    $title = $faker->sentence;
    $slug = str_slug($title);
    return [
        'title' => $title,
        'excerpt' => $faker->sentence,
        'content' => $faker->realText(600),
        'slug' => $slug,
        'user_id' => User::all()->random()->id, // password
        'status' => $approved = $faker->randomElement([Article::APPROVED_ARTICLE, Article::UNAPPROVED_ARTICLE]),
        'image' => $faker->randomElement(['1.jpg', '2.jpg', '3.jpg']),
        'published_at' => $approved == Article::APPROVED_ARTICLE ? now() : null,
        'comment_count' => $faker->randomNumber($nbDigits = 2),

    ];
});

/**
 * Return Comments factory
 *
 * @return array
 */

$factory->define(Comment::class, function (Faker $faker) {

    return [
        'article_id' => Article::all()->random()->id,
        'user_id' => User::all()->random()->id,
        'comment_body' => $faker->sentence,
        'status' => $faker->randomElement([Comment::APPROVED_COMMENT, Comment::UNAPPROVED_COMMENT]),
        'article_id' => Article::all()->random()->id,

    ];
});