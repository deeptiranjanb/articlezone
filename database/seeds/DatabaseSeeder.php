<?php



use App\Model\User;
use App\Model\Article;
use App\Model\Comment;
use App\Model\Category;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        User::truncate();
        Article::truncate();
        Comment::truncate();
        Category::truncate();
        DB::table('article_category')->truncate();

        User::flushEventListeners();
        Article::flushEventListeners();
        Comment::flushEventListeners();
        Category::flushEventListeners();

        $userQuantity = 5;
        $articleQuantity = 50;
        $categoryQuantity = 5;
        $commentQuantity = 150;


        // $this->call(UsersTableSeeder::class);

        factory(User::class, $userQuantity)->create();
        factory(Category::class, $categoryQuantity)->create();

        factory(Article::class, $articleQuantity)->create()->each(
            function ($article) {
                $catagories = Category::all()->random(mt_rand(1, 5))->pluck('id');
                $article->categories()->attach($catagories);
            }
        );

        factory(Comment::class, $commentQuantity)->create();
    }
}