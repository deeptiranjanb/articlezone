@component('mail::message')

A new comment has been added to your article.Please check it out

@component('mail::button', ['url' => 'http://articlezone-react.s3-website.ap-south-1.amazonaws.com/signin'])
Sign In
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent